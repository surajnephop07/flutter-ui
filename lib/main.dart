import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlueAccent,
//
//This Column(Widget) is Canvas
//
      body: Stack(
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
//
//container(widget) for icons and doctors starts......................(Element of canvas No:1)
//

          Container(
              padding: const EdgeInsets.only(
                  top: 60.0, left: 30.0, right: 30.0, bottom: 30.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                // ignore: prefer_const_literals_to_create_immutables
                children: <Widget>[
                  const Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                  const Text(
                    ' Doctors',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  const SizedBox(width: 150),
                  const Icon(
                    IconData(0xe1b8, fontFamily: 'MaterialIcons'),
                    color: Colors.white,
                  ),
                ],
              )),
//
//container(widget) for icons and doctors ends......................
//

//
// container(widget) for search bar and icons starts......................(Element of canvas No:2)
//

          Container(
              margin: const EdgeInsets.fromLTRB(0, 100, 0, 0),
              child: Stack(
                children: [
                  // 1st child of Stack as Container...........
                  Container(
                      child: TextField(
                        decoration: InputDecoration(
                          enabledBorder: const OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromARGB(255, 7, 7, 7),
                                width: 1.0),
                          ),
                          prefixIcon: const Icon(Icons.search),
                          suffixIcon: IconButton(
                            icon: const Icon(Icons.clear),
                            onPressed: () {
                              /* Clear the search field */
                            },
                          ),
                          hintText: 'Search....',
                        ),
                      ),
                      //...lavaris...........
                      padding: const EdgeInsets.fromLTRB(5, 50, 5, 150),
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.0),
                          topRight: Radius.circular(20.0),
                        ),
                      )),
                  //...lavaris...........

// 2nd child of Stack as Container

                  Container(
                      margin: const EdgeInsets.fromLTRB(10, 170, 10, 10),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              child: const Icon(Icons.bloodtype_outlined,
                                  size: 40,
                                  color: Color.fromARGB(255, 18, 18, 18)),
                              width: 70.0,
                              height: 70.0,
                              decoration: const BoxDecoration(
                                color: Color.fromARGB(255, 198, 82, 72),
                              ),
                            ),
                            Container(
                              child: const Icon(Icons.water_drop_outlined,
                                  size: 40,
                                  color: Color.fromARGB(255, 18, 18, 18)),
                              width: 70.0,
                              height: 70.0,
                              decoration: const BoxDecoration(
                                color: Color.fromARGB(255, 198, 82, 72),
                              ),
                            ),
                            Container(
                              child: const Icon(Icons.person_add_alt_outlined,
                                  size: 40,
                                  color: Color.fromARGB(255, 18, 18, 18)),
                              width: 70.0,
                              height: 70.0,
                              decoration: const BoxDecoration(
                                color: Color.fromARGB(255, 198, 82, 72),
                              ),
                            ),
                            Container(
                              child: const Icon(Icons.message_outlined,
                                  size: 40,
                                  color: Color.fromARGB(255, 18, 18, 18)),
                              width: 70.0,
                              height: 70.0,
                              decoration: const BoxDecoration(
                                color: Color.fromARGB(255, 198, 82, 72),
                              ),
                            ),
                            // ),
                          ]))
                ],
              )),

//
// container(Widget) for search bar and icons ends......................
//

          //
// Listview (Widget) for card starts......................(Element of canvas No:3)
//

          Container(
            margin: const EdgeInsets.fromLTRB(10, 390, 10, 2),
            width: 400,
            height: 100,
            padding: const EdgeInsets.all(10.0),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              color: const Color.fromARGB(255, 255, 255, 255),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: const <Widget>[
                  ListTile(
                    leading: Icon(Icons.person_rounded, size: 60),
                    title: Text(
                      'Ram Bahadur Bir',
                      style: TextStyle(fontSize: 15.0),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(10, 480, 10, 2),
            width: 400,
            height: 100,
            padding: const EdgeInsets.all(10.0),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              color: const Color.fromARGB(255, 253, 252, 251),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: const <Widget>[
                  ListTile(
                    leading: Icon(Icons.person_rounded, size: 60),
                    title: Text('Ram Bahadur Bir',
                        style: TextStyle(fontSize: 15.0)),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(10, 570, 10, 2),
            width: 400,
            height: 100,
            padding: const EdgeInsets.all(10.0),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              color: const Color.fromARGB(255, 253, 252, 252),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: const <Widget>[
                  ListTile(
                    leading: Icon(Icons.person_rounded, size: 60),
                    title: Text('Ram Bahadur Bir',
                        style: TextStyle(fontSize: 15.0)),
                  ),
                ],
              ),
            ),
          ),
//
// Listview (Widget) for card ends......................
//
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined),
            label: 'Home',
            backgroundColor: Colors.red,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.folder_outlined),
            label: 'Folder',
            backgroundColor: Colors.green,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book_outlined),
            label: 'Book',
            backgroundColor: Colors.purple,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline),
            label: 'Profile',
            backgroundColor: Colors.pink,
          ),
        ],
      ),
    );
  }
}
